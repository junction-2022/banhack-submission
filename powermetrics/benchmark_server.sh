#!/usr/bin/env bash

# Purpose of this script is to benchmark hacksome backend/data storage
# performance

set -euxo pipefail

CSV_FILE=vegeta.csv
VEGETA_OUT=$(mktemp)

python3 gentargets.py > requests.json

vegeta attack -format=json -rate=100 -duration=1s < requests.json > "$VEGETA_OUT"
vegeta encode --to=csv "$VEGETA_OUT" > "$CSV_FILE"
Rscript analyze.R "$CSV_FILE"
