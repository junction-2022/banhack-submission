# Purpose of this script is to generate target urls for stress test.
import ndjson

targets = [
    {"method": "GET", "url": "http://0.0.0.0:8000/gimp/2.10/"},
    {"method": "GET", "url": "http://0.0.0.0:8000/out.html"},
]

print(ndjson.dumps(targets))
