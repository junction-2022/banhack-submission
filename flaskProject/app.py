
# Imports
import json
import sqlite3
import datetime


# TODO: Use markupsafe to parse user input.
from flask import Flask
from flask import render_template
from flask import request

from models import post

# Create database and its tables if they don't exist yet. #
DB_NAME = "banhack.db"

# Creates implicitly the database if it doesn't exist.
databaseConnection: sqlite3.Connection = sqlite3.connect(DB_NAME, check_same_thread=False)

# posts table
databaseConnection.execute(
    "CREATE TABLE IF NOT EXISTS posts("
        # post_id is an alias to rowid column.
        "post_id INTEGER NOT NULL PRIMARY KEY,"
        # User id that can be associated to a pubkey.
        "user_id INTEGER NOT NULL UNIQUE,"
        # comment timestamp in ISO 8601 format.
        "comment_timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,"
        # parent_post_id is always a valid post id.
        "parent_post_id INTEGER,"
        # Media content file on the server.
        "post_media_content TEXT,"
        # Post text content.
        "post_content TEXT NOT NULL,"
        # post_score
        "post_score INTEGER NOT NULL"
    ");"
    )

# users table
databaseConnection.execute(
    "CREATE TABLE IF NOT EXISTS users("
        # user_id is an alias to rowid column.
        "user_id INTEGER NOT NULL PRIMARY KEY,"
        # user_name
        "user_name TEXT NOT NULL UNIQUE,"
        # user_pubkey public key for user posts.
        "user_pubkey TEXT NOT NULL"
    ");"
    )


# TODO: Test.
def get_all_posts() -> list[post.Post]:
    postquery = databaseConnection.execute(
        "SELECT p.post_content, p.comment_timestamp, u.user_name FROM posts p INNER JOIN users u ON p.user_id = u.user_id"
    ).fetchall()

    all_posts = []

    for post_tuple in postquery:
        all_posts.append(post.Post("", post_tuple[2], post_tuple[1], post_tuple[1], "", post_tuple[0], "", ""))

    return all_posts


def get_post(post_id: int) -> post.Post:
    query = databaseConnection.execute(
        f"SELECT p.post_content, p.comment_timestamp, u.user_name FROM posts p INNER JOIN users u ON p.user_id = u.user_id WHERE p.post_id = { post_id }"
    ).fetchone()

    if query is None:
        return None

    return post.Post("", query[2], query[1], query[1], "", query[0], "", "")

# FIXME: comments-table doesn't exist anymore.
def insert_comment(comment: str, parent_post_id: int, parent_comment_id: int):
    # TODO: Bottleneck: re-opening same file over and over..
    with open("sql/insert_comment.sql") as sqlFile:
        comment_insertion_command = sqlFile.read()
    databaseConnection.execute(comment_insertion_command,
                               (comment, get_iso8601_timestamp(), parent_post_id, parent_comment_id))
    databaseConnection.commit()


def username_exists(username: str) -> bool:
    username in [u for u, in databaseConnection.execute("SELECT DISTINCT user_name FROM posts;").fetchall()]


def get_iso8601_timestamp() -> str:
    return datetime.datetime.now().isoformat()


# TODO:
def get_previous_post_id(current_id: int) -> int:
    current_id - 1 if current_id != 0 else 0


# TODO:
def get_next_post_id(current_id: int) -> int:
    last_post_id, = databaseConnection.execute("SELECT MAX(user_id) FROM posts;").fetchall().pop()
    current_id + 1 if current_id + 1 <= last_post_id else last_post_id


# Start the app. #
app = Flask(__name__)


@app.route('/')
def index_page():
    return render_template("index.html", posts=get_all_posts(), header__linkPrevious="",
                           header__title="BanHack", header__linkNext="")


@app.get('/post/<int:post_id>')
def post_get(post_id: int):
    post_object = get_post(post_id)
    if post_object is None:
        return "404 not found", 404

    return render_template("post.html", post=post_object)


@app.post('/post/')
def rest_post():
    return "TODO"


@app.post('/exists/')
def pubkey_exists() -> str:
    """
    Client can query whether a certain pubkey exists within the server.
    """
    try:
        pubkey = request.form["pubkey"]
    except KeyError:
        return json.dumps({ "valid_query" : False })

    # TODO: Validate user input. XD

    pubkey_found: bool = pubkey in\
                         [post for post, in databaseConnection.execute("SELECT user_pubkey FROM users;").fetchall()]

    json.dumps({ "exists" : pubkey_found })


@app.post('/join/')
def join_post():
    def json_response(valid_username: bool = False, reason_username: str = "",
                      valid_pubkey: bool = False, reason_pubkey: str = ""):
        return json.dumps({
            "username": {"valid": valid_username, reason_username: ""},
            "pubkey": {"valid": valid_pubkey, reason_pubkey: ""}
        })

    try:
        username = request.form['username']
    except KeyError:
        return json_response(False, "Missing username.")

    try:
        pubkey = request.form['pubkey']
    except KeyError:
        return json_response(False, "", False, "Missing pubkey.")

    # Validate username.
    if len(username) > 12 or len(username) < 4:
        return json_response(False, f"Username length should be between 4 and 12 characters. Your was { len(username) }.")
    elif any(str.isalnum(c) for c in username):
        return json_response(False, f"Username should contain only alphanumeric characters.")

    # TODO: Validate pubkey :D

    # TODO: Tunge tietokantaan ilman kunnon input sanitointia XD
    databaseConnection.execute(
        f"INSERT INTO user(user_name, user_pubkey) VALUES('{ username }', '{ pubkey }')"
    )

    return json_response(True, "", True, "")


#if __name__ == '__main__':
#    app.run()
