
class Author:
    href: str
    username: str


class Post:
    author: Author = Author()

    def __init__(self, auth_href: str, auth_username: str, date_time: str,
                 date_human: str, href: str, content: str, comment_href: str, comment_count: str):
        self.author.href = auth_href
        self.author.username = auth_username
        self.dateTime = date_time
        self.dateHuman = date_human
        self.href = href
        self.content = content
        self.commentHref = comment_href
        self.commentCount = comment_count
