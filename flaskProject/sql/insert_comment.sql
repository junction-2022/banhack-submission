insert into comments (
  comment,
  comment_timestamp,
  parent_post_id,
  parent_comment_id
) values (
  ?,
  ?,
  ?,
  ?
)
