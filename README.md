# CGI Green CMS - banhack

## TODO

### SQLite
- [ ] Mitä kenttiä tarvitaan?
- [ ] Mitä tietotyyppejä?


# Example: writing new comments into database

```python
with open("sql/insert_comment.sql") as f:
    insert_comment = f.read()
conn.execute(insert_comment, (1, "kommentti", "20220303T10:10", 1, 1))
conn.commit()
```


# Käpy specification

## What is Käpy?

Käpy provides score/karma system in Banhack. Käpy provides multiple purposes:

1. It provides classic "like" system similar to other social medias - users can collect points and become internet famous.

1. It incentivizes users to make energy-efficient choices, such as using more energy-efficient video sizes, image formats etc.

1. It incentivizes users to not spend too much time on the social media - this way we reduce social media addiction, which is becoming larger problem year by year.

## How do users collect Käpy?

* Users get 100 Käpy when they register to Banhack

* Users get Käpy when other people like their posts or comments.

## How do users spend Käpy?

* Posting high definition videos and images consumes Käpy

* Spending long periods of time on Banhack will consume Käpy

## Flask / Jinja templates quickstart

https://flask.palletsprojects.com/en/2.2.x/tutorial/templates/
