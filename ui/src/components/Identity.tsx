import type { FunctionComponent } from 'preact'
import { useState } from 'preact/hooks'
import { ArrowLeft } from 'lucide-preact'
import { privateKey as storedPrivateKey, passphrase as storedPassphrase, username as storedUsername } from '../stores/identity'
import * as openpgp from 'openpgp'

const Identity: FunctionComponent = () => {
  const [state, setState] = useState<'new' | 'import' | 'greeting'>('greeting')
  const [error, setError] = useState<string | null>(null)

  const handleGeneration = async (event: Event): Promise<void> => {
    event.preventDefault()

    const username = (event.target as HTMLFormElement).username.value
    const passphrase = (event.target as HTMLFormElement).passphrase.value

    if (typeof username !== 'string' || username.length < 1) {
      setError('Username missing')
      return
    }

    const { privateKey, publicKey, revocationCertificate } = await openpgp.generateKey({
      type: 'ecc',
      curve: 'curve25519',
      userIDs: [{ name: username }],
      format: 'armored',
      passphrase: passphrase
    })

    console.log(privateKey) // '-----BEGIN PGP PRIVATE KEY BLOCK ... '
    console.log(publicKey) // '-----BEGIN PGP PUBLIC KEY BLOCK ... '
    console.log(revocationCertificate) // '-----BEGIN PGP PUBLIC KEY BLOCK ... '

    storedPrivateKey.set(privateKey)
    storedPassphrase.set(passphrase)
    storedUsername.set(username)
  }

  return (
    <div class='w-full border-b flex flex-col md:flex-row'>
      <div class='border-b md:border-b-0 md:border-r p-4 md:p-8 w-full md:w-1/3 shrink-0 flex md:justify-end'>
        <h2 class='text-2xl font-semibold'>Identity</h2>
      </div>
      <div class='p-4 md:p-8 flex flex-col gap-4 md:gap-8'>
        {error !== null && <div class='w-full bg-yellow-400 bg-opacity-50 p-4 rounded-sm'>{error}</div>}
        {
            {
              new: (
                <>
                  <div>
                    <button class='btn -transparent -icon mb-2' onClick={() => setState('greeting')}><ArrowLeft class='w-4 h-auto' />Back to choise</button>
                    <h3 class='mb-2 text-lg'>Creating new identity, exciting!</h3>
                    <p>Provide username (unique, we will check this) and passphrase. Passphrase is like password, but it used to secure your private key, if you lose one, you lose access to your signature. Private key is stored your browser, and passphrase in session storage.</p>
                  </div>
                  <form class='flex flex-col gap-4 w-full max-w-xl' onSubmit={handleGeneration}>
                    <label class='flex flex-col gap-2'>
                      Username
                      <input type='text' autoComplete='off' name='username' required />
                    </label>
                    <label class='flex flex-col gap-2'>
                      Passphrase (optional)
                      <input type='password' autoComplete='off' name='passphrase' />
                    </label>
                    <button class='btn' type='submit'>
                      Create identity
                    </button>
                  </form>
                </>
              ),
              import: (
                <>
                  <div>
                    <button class='btn -transparent -icon mb-2' onClick={() => setState('greeting')}><ArrowLeft class='w-4 h-auto' />Back to choise</button>
                    <h3 class='mb-2 text-lg'>Importing identity</h3>
                    <p>Welcome back, long time no see! Give your private key, and passphrase if you have one, we take care of the rest.</p>
                  </div>
                  <div class='flex flex-col gap-4 w-full max-w-xl'>
                    <label class='flex flex-col gap-2'>
                      Private key
                      <textarea spellCheck={false} />
                    </label>
                    <label class='flex flex-col gap-2'>
                      Passphrase (optional)
                      <input type='password' autoComplete='off' />
                    </label>
                    <button class='btn'>
                      Auhenticate
                    </button>
                  </div>
                </>
              ),
              greeting: (
                <>
                  <div>
                    <h3 class='mb-2 text-lg'>Greetings!</h3>
                    <p>Nice to see you here, for identity we use OpenPGP. You can either create new identity or import your old one to keep you username.</p>
                  </div>
                  <div class='flex flex-col md:flex-row gap-8 justify-between md:justify-start items-center w-full'>
                    <button class='btn' onClick={() => setState('new')}>
                      Create new identity
                    </button>
                    <button class='btn' onClick={() => setState('import')}>
                      Import identity
                    </button>
                  </div>
                </>
              )
            }[state]
        }
      </div>
    </div>
  )
}

export default Identity
