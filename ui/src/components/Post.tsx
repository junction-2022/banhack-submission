import type { FunctionComponent } from 'preact'
import { Hop, MessageCircle } from 'lucide-preact'

const Post: FunctionComponent = () => {
  return (
    <article class='border-b p-8 pb-4 transition hover:bg-neutral-400 dark:hover:bg-neutral-800 hover:bg-opacity-10'>
      <a href='{{ post.author.href }}'>
        <header class='text-lg flex items-center group gap-4 pb-4'>
          {'{{ post.author.username }}'}
          <time dateTime='{{ post.dateTime }}' class='ml-auto text-base opacity-75 transition-opacity group-hover:opacity-100'>{'{{ post.dateHuman }}'}</time>
        </header>
      </a>
      <a href='{{ post.href }}'>
        <p>{'{{ post.content }}'}</p>
      </a>
      <footer class='-mx-8 border-t border-opacity-20 border-t-white px-8 pt-5 mt-8 flex gap-8'>
        <a href='{{ post.commentHref }}' class='flex items-center justify-center relative group'>
          <span class='aspect-square p-2 md:p-3 transition hover:bg-neutral-200 dark:hover:bg-neutral-800 rounded-full'>
            <MessageCircle class='h-auto w-4 sm:w-6' />
          </span>
          {'{{ post.commentCount }}'}
        </a>
        <button class='flex items-center justify-center relative group'>
          <span class='aspect-square p-2 md:p-3 transition hover:bg-neutral-200 dark:hover:bg-neutral-800 rounded-full'>
            <Hop class='h-auto w-4 sm:w-6' />
          </span>
          100k
        </button>
      </footer>
    </article>
  )
}

export default Post
