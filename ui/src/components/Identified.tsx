import { useStore } from '@nanostores/preact'
import type { FunctionComponent } from 'preact'
import { username, clearStorage, passphrase } from '../stores/identity'
import { Eraser } from 'lucide-preact'

const Identified: FunctionComponent = () => {
  const $username = useStore(username)
  const $passphrase = useStore(passphrase)
  return (
    <div class='w-full border-b flex flex-col md:flex-row'>
      <div class='border-b md:border-b-0 md:border-r p-4 md:p-8 w-full md:w-1/3 shrink-0 flex md:justify-end'>
        <h2 class='text-2xl font-semibold'>Identity</h2>
      </div>
      <div class='p-4 md:p-8 flex flex-col gap-4 md:gap-8'>
        <div>
          <h3 class='mb-2 text-lg'>Hello, <span class='capitalize'>{$username}</span>!</h3>
          {
          $passphrase === undefined
            ? (
              <>
                <p>You need to enter passphrase to sent any messages or any interact.</p> {/* TBD */}
              </>
              )
            : (
              <p>You have identified your self, great! Passphrase is stored session storage, when you close browser it will be cleared.</p>
              )
        }
        </div>
        <div class='flex flex-col md:flex-row gap-8 justify-between md:justify-start items-center w-full'>
          <button class='btn -icon' onClick={() => clearStorage()}>
            <Eraser class='w-4 h-auto' />Clear browser storage
          </button>
        </div>
      </div>
    </div>
  )
}

export default Identified
