import type { FunctionComponent } from 'preact'
import { Fingerprint, Gitlab, Hop, Home, Send } from 'lucide-preact'
import { useStore } from '@nanostores/preact'
import { modal } from '../stores/modal'
import { username } from '../stores/identity'

interface MenuProps {
  index?: boolean
}

const Menu: FunctionComponent<MenuProps> = () => {
  const $modal = useStore(modal)
  const $username = useStore(username)
  return (
    <nav class='border-r h-screen sticky top-0'>
      <ul class='p-2 md:p-4 flex flex-col gap-2 md:gap-4'>
        <li>
          <a href='/' class='p-4 md:p-6 aspect-square flex items-center justify-center transition hover:bg-neutral-200 dark:hover:bg-neutral-800 rounded-full relative group'>
            <Home class='h-auto w-4 sm:w-6' />
            <span class='opacity-0 transition-opacity absolute text-xs bottom-0 group-hover:opacity-100 select-none'>Index</span>
          </a>
        </li>
        {
          $username !== undefined && (
            <li>
              <button onClick={() => modal.set($modal === 'post' ? false : 'post')} class='p-4 md:p-6 aspect-square flex items-center justify-center transition hover:bg-neutral-200 dark:hover:bg-neutral-800 rounded-full relative group'>
                <Send class='h-auto w-4 sm:w-6' />
                <span class='opacity-0 transition-opacity absolute text-xs bottom-0 group-hover:opacity-100 select-none'>Post</span>
              </button>
            </li>
          )
        }
        {
          $username !== undefined && (
            <li>
              <button onClick={() => modal.set($modal === 'credit' ? false : 'credit')} class='p-4 md:p-6 aspect-square flex items-center justify-center transition hover:bg-neutral-200 dark:hover:bg-neutral-800 rounded-full relative group'>
                <div class='text-xs md:text-sm font-semibold relative -mb-2 -mr-2'>
                  +99<Hop class='w-3 sm:w-4 h-auto absolute opacity-50 -top-2 -left-2' />
                </div>
                <span class='opacity-0 transition-opacity absolute text-xs bottom-0 group-hover:opacity-100 select-none'>Credit</span>
              </button>
            </li>
          )
        }
        <li>
          <button onClick={() => modal.set($modal === 'identity' ? false : 'identity')} class='p-4 md:p-6 aspect-square flex items-center justify-center transition hover:bg-neutral-200 dark:hover:bg-neutral-800 rounded-full relative group'>
            <Fingerprint class='h-auto w-4 sm:w-6' />
            <span class='opacity-0 transition-opacity absolute text-xs bottom-0 group-hover:opacity-100 select-none'>Identity</span>
          </button>
        </li>
        <li>
          <a href='https://gitlab.com/junction-2022/cgi-green-cms/' class='p-4 md:p-6 aspect-square flex items-center justify-center transition hover:bg-neutral-200 dark:hover:bg-neutral-800 rounded-full relative group'>
            <Gitlab class='h-auto w-4 sm:w-6' />
            <span class='opacity-0 transition-opacity absolute text-xs bottom-0 group-hover:opacity-100 select-none'>Source</span>
          </a>
        </li>
      </ul>
    </nav>
  )
}

export default Menu
