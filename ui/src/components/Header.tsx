import type { FunctionComponent } from 'preact'
import { ArrowLeft, ArrowRight } from 'lucide-preact'

const Header: FunctionComponent = () => {
  return (
    <header class='p-2 md:p-4 flex justify-between items-center gap-2 md:gap-4 text-lg md:text-xl border-b'>
      <a href='{{ header__linkPrevious }}' class='p-4 md:p-6 aspect-square flex items-center justify-center transition hover:bg-neutral-200 dark:hover:bg-neutral-800 rounded-full relative group'>
        <ArrowLeft class='h-auto w-4 sm:w-6' />
      </a>
      <a href='{{ header__titleHref }}'><h1>{'{{ header__title }}'}</h1></a>
      <a href='{{ header__linkNext }}' class='p-4 md:p-6 aspect-square flex items-center justify-center transition hover:bg-neutral-200 dark:hover:bg-neutral-800 rounded-full relative group'>
        <ArrowRight class='h-auto w-4 sm:w-6' />
      </a>
    </header>
  )
}

export default Header
