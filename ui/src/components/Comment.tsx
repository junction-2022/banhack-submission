import type { FunctionComponent } from 'preact'
import { Hop } from 'lucide-preact'

const Comment: FunctionComponent = () => {
  return (
    <article class='border-b p-8 pb-4 transition hover:bg-neutral-400 dark:hover:bg-neutral-800 hover:bg-opacity-10 border-x'>
      <a href='{{ comment.author.href }}'>
        <header class='text-lg flex items-center group gap-4 pb-4'>
          {'{{ comment.author.username }}'}
          <time dateTime='{{ comment.dateTime }}' class='ml-auto text-base opacity-75 transition-opacity group-hover:opacity-100'>{'{{ comment.dateHuman }}'}</time>
        </header>
      </a>
      <a href='{{ comment.href }}'>
        <p>{'{{ comment.content }}'}</p>
      </a>
      <footer class='-mx-8 border-t border-opacity-20 border-t-white px-8 pt-5 mt-8 flex gap-8'>
        <button class='flex items-center justify-center relative group'>
          <span class='aspect-square p-2 md:p-3 transition hover:bg-neutral-200 dark:hover:bg-neutral-800 rounded-full'>
            <Hop class='h-auto w-4 sm:w-6' />
          </span>
          100k
        </button>
      </footer>
    </article>
  )
}

export default Comment
