import type { FunctionComponent } from 'preact'
import { useStore } from '@nanostores/preact'
import { modal } from '../stores/modal'
import { username } from '../stores/identity'
import Identity from './Identity'
import Identified from './Identified'

const Modal: FunctionComponent = () => {
  const $modal = useStore(modal)
  const $username = useStore(username)

  switch ($modal) {
    case 'credit':
      return (
        <div class='w-full border-b flex flex-col md:flex-row'>
          <div class='border-b md:border-b-0 md:border-r p-4 md:p-8 w-full md:w-1/3 shrink-0 flex md:justify-end'>
            <h2 class='text-2xl font-semibold'>Credit</h2>
          </div>
          <div class='p-4 md:p-8'>
            Some many credits ...
          </div>
        </div>
      )
    case 'identity':
      return $username === undefined ? <Identity /> : <Identified />
    case 'post':
      return (
        <div class='w-full border-b flex flex-col md:flex-row'>
          <div class='border-b md:border-b-0 md:border-r p-4 md:p-8 w-full md:w-1/3 shrink-0 flex md:justify-end'>
            <h2 class='text-2xl font-semibold'>Post</h2>
          </div>
          <div class='p-4 md:p-8'>
            Coming soon
          </div>
        </div>
      )
    default:
      break
  }
  return null
}

export default Modal
