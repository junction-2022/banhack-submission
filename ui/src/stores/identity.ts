import { persistentAtom } from '@nanostores/persistent'
import { atom, onMount, onSet } from 'nanostores'

export const privateKey = persistentAtom<string | undefined>('__private-key', undefined)

export const username = persistentAtom<string | undefined>('__private-key', undefined)

export const passphrase = atom<string | undefined>(undefined)

onMount(passphrase, () => {
  const passphraseFromSession = window.sessionStorage.getItem('__passphrase')
  if (passphraseFromSession !== null) {
    passphrase.set(passphraseFromSession)
  }
})

onSet(passphrase, ({ newValue, abort }) => {
  if (newValue === undefined) {
    window.sessionStorage.removeItem('__passphrase')
    return
  }
  window.sessionStorage.setItem('__passphrase', newValue)
})

export const clearStorage = (): void => {
  privateKey.set(undefined)
  username.set(undefined)
  passphrase.set(undefined)
}
