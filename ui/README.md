# Junction 2022

Remember to use {{ }}, when targeting variables.
> Example: `{{ header__linkPrevious }}`


- [x] `page__title` Page title in meta
- [x] Header element
    - [x] `header__linkPrevious` Previous href link
    - [x] `header__linkNext` Next href link
    - [x] `header__title` Center title, h1
    - [x] `header__titleHref` Link for page header title
- [x] Posts page
    - [x] `for post in posts` loops `posts`
        - [x] `post.author.href`
        - [x] `post.author.username`
        - [x] `post.dateTime`
        - [x] `post.dateHuman`
        - [x] `post.href` link to comment page
        - [x] `post.content` is inside `p`
        - [x] `post.commentHref`
        - [x] `post.commentCount`
- [x] Post page
    - [x] `post.author.href`
    - [x] `post.author.username`
    - [x] `post.dateTime`
    - [x] `post.dateHuman`
    - [x] `post.href` link to comment page
    - [x] `post.content` is inside `p`
    - [x] `post.commentHref`
    - [x] `post.commentCount`
    - [x] `for comment in comments` loops `comments`
        - [x] `comment.author.href`
        - [x] `comment.author.username`
        - [x] `comment.dateTime`
        - [x] `comment.dateHuman`
        - [x] `comment.href` link to comment with anchor
        - [x] `comment.content` is inside `p`

[![JavaScript Style Guide](https://cdn.rawgit.com/standard/standard/master/badge.svg)](https://github.com/standard/standard)